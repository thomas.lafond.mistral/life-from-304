# Life from 304

Dépôt visant à répertorier, organiser la transmission de savoir en mathématiques pour la classe de 304.

## Activités

## Nombres et Calculs

- [La légende de l'échiquier](https://framagit.org/thomas.lafond.mistral/echiquier) *[puissances]*



## Documents 

### Ressources extérieur

- [doc/ressources.md](https://framagit.org/thomas.lafond.mistral/life-from-304/blob/master/doc/ressources.md)

### Orientations extérieurs

- [doc/orentiation.md](https://framagit.org/thomas.lafond.mistral/life-from-304/blob/master/doc/orientation.md)

## Licence

Les fichiers sont disponibles sous la licence :  [CC-BY-SA](http://creativecommons.org/licenses/by-sa/3.0/legalcode).
La licence **CC-BY-SA** est une licence libre, copyleft qui impose la **Paternité** -BY et le **Partage des conditions initiales à l'identique** -SA.

#### Contributeurs

* LAFOND Thomas : https://framagit.org/thomas.lafond.mistral
