# Orientation et culture


## Événements extérieurs

#### La maison des mathématiques et de l’informatique

http://www.mmi-lyon.fr/

> La maison des mathématiques et de l’informatique a commencé ses activités à la rentrée 2012. Elle propose des expositions, des ateliers et stands d’activités mathématiques ainsi que des conférences. Elle a également pour vocation de fédérer, d’organiser et d’amplifier les diverses actions de diffusion mathématique et informatique qui ont lieu à Lyon et dans sa région.

#### Le cafe statistique lyonnais

http://cafestatistiquelyonnais.blogspot.fr/

> Des soirées-débats publiques ouvertes à tous, des débats qui évitent la technicité, des thèmes très variés et d'actualité, un dialogue entre des statisticiens et le public le plus large : simples citoyens, non-spécialistes, représentants de professions diverses ou participants au débat politique, économique ou social. 
